from django.db import models


class yangilik(models.Model):
    icon = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    text = models.TextField()
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Yangiliklar"



class teacher(models.Model):
    name = models.CharField(max_length=30)
    image = models.ImageField(upload_to='media')
    darajasi = models.CharField(max_length=50)
    text = models.TextField()
    telegram = models.CharField(max_length=100)
    facebook = models.CharField(max_length=100)
    instagram = models.CharField(max_length=100)
    twitter = models.CharField(max_length=100)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "O`qituvchilar"

class contact(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField()
    subject = models.TextField(null=True, blank=True)
    message = models.TextField()
    def __str__(self):
      return self.name
    class Meta:
        verbose_name_plural = "Xabarlar"


class fikr(models.Model):
    image = models.ImageField(upload_to='media')
    text = models.TextField()
    name = models.CharField(max_length=30)
    kimligi = models.CharField(max_length=30)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "O`quvchilar fikri"


class raqam(models.Model):
    son = models.IntegerField()
    nomi = models.CharField(max_length=50)
    def __str__(self):
        return self.nomi
    class Meta:
        verbose_name_plural = "Statistika"


class course(models.Model):
    image = models.ImageField(upload_to='media')
    davomiyligi = models.CharField(max_length=30)
    fan_nomi = models.CharField(max_length=50)
    text = models.TextField()
    def __str__(self):
        return self.fan_nomi
    class Meta:
        verbose_name_plural = "Kurslar ro`yxati"


class courses(models.Model):
    image = models.ImageField(upload_to='media')
    davomiyligi = models.CharField(max_length=30)
    fan_nomi = models.CharField(max_length=50)
    text = models.TextField()
    def __str__(self):
        return self.fan_nomi
    class Meta:
        verbose_name_plural = "Qo`shimcha kurslar ro`yxati"



class blog(models.Model):
    image = models.ImageField(upload_to='media')
    note = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    text = models.TextField()
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Bloglar ro`yxati"

class event(models.Model):
    image = models.ImageField(upload_to='media')
    note = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    text = models.TextField()
    vaqt = models.CharField(max_length=50)
    joyi = models.CharField(max_length=50)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Tadbirlar"

class about(models.Model):
    image = models.ImageField(upload_to='media')
    title = models.CharField(max_length=70)
    text = models.TextField()
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Maktab haqida"

class galery(models.Model):
    image = models.ImageField(upload_to='media')
    title = models.CharField(max_length=50)
    text = models.TextField()
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Galereya"


class contact1(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField()
    message = models.TextField()
    def __str__(self):
      return self.name
    class Meta:
        verbose_name_plural = "O`qituvchiga kelgan xabarlar"








