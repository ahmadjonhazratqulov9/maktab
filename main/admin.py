from django.contrib import admin
from .models import *


admin.site.register(yangilik)
admin.site.register(teacher)
admin.site.register(contact)
admin.site.register(fikr)
admin.site.register(raqam)
admin.site.register(course)
admin.site.register(blog)
admin.site.register(event)
admin.site.register(about)
admin.site.register(courses)
admin.site.register(galery)
admin.site.register(contact1)